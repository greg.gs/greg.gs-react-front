# Changelog
*All notable changes to this project will be documented in this file.*

## [0.1.0] - Coming...
### Features
- Initializing of the project structure and configuration.
### Bug Fixes
### Changes
- Adding the changelog modification in the tasks to complete in the merge request template.